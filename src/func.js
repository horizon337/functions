function getSum(str1, str2) {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }

  const sumArr = str1.split('');
  const newArr = sumArr.map(Number);

  const sumArr2= str2.split('');
  const newArr2 = sumArr2.map(Number);

  const sum =  newArr2.map(function (num, ind) {
      return num + newArr[ind];
  });

  for (const elem of sum) {
    if (Number.isNaN(elem) ) {
      return false;
  }
  }
  return sum.join('');
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  if (Array.isArray(listOfPosts)) {
    let postsCount = 0;
    let commentsCount = 0;
    for (const post of listOfPosts) {
      if(post.author == authorName) {
        postsCount++;
      }
      let comments = post.comments;
      if(comments != undefined) {
      comments.forEach(com => {
        if(com.author == authorName){
          commentsCount++;
        }
        });
      }
    }
    return `Post:${postsCount},comments:${commentsCount}`;
  }
};

function tickets(people){
  var a25 = 0,a50 = 0;
  for(const person of people){
    if(person == 25){
      a25 += 1;
    }
    if(person == 50){
      a25 -= 1; a50 += 1;
    }
    if(person == 100){
      if(a50 == 0 && a25 >= 3){
        a25 -= 3;
      }else{
        a25 -= 1; a50 -= 1;
      }
    }
    if(a25 < 0 || a50 < 0){
       return 'NO';
    }
  }
  return 'YES';
}

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
